# Mail validation regex

Lot of things todo...  
But the idea is to figure out a proper regex expresssion for e-mail address validation.

**AND** to provide the tools in different languages to use this regex.

It should follow the RFC-5322 standard as described here:  
<https://en.wikipedia.org/wiki/Email_address>

Right now, there's just this README file.


## PHP

Based on Regex I found here: <https://regex101.com/library/iT4vI4>  
But was missing Unicode support.

### Pattern

`/^(?<full>(?<local>[\w!#$%&\'*+\-\/=?^_`{|}~]+(?:\.[\w!#$%&\'*+\-\/=?^_`{|}~]+)*|"[\w!#$%&\'*+\-\/=?^_`{|}~(),:;<>@\[\] \\\\"]+(?:\.[\w!#$%&\'*+\-\/=?^_`{|}~(),:;<>@\[\] \\\\"]+)*")@(?<domain>[[:alnum:]-]+(?:\.[[:alnum:]-]+)*|\[[0-9.]+\]|\[IPv6:[[:xdigit:]:]+\]))$/imu`

### Usage example

```php
<?php

    function isValidEmail($email)
    {
        $pattern = '/^(?<full>(?<local>[\w!#$%&\'*+\-\/=?^_`{|}~]+(?:\.[\w!#$%&\'*+\-\/=?^_`{|}~]+)*|"[\w!#$%&\'*+\-\/=?^_`{|}~(),:;<>@\[\] \\\\"]+(?:\.[\w!#$%&\'*+\-\/=?^_`{|}~(),:;<>@\[\] \\\\"]+)*")@(?<domain>[[:alnum:]-]+(?:\.[[:alnum:]-]+)*|\[[0-9.]+\]|\[IPv6:[[:xdigit:]:]+\]))$/imu';

        return (preg_match($pattern, $f) === 1) ? true : false;
    }
```

## JavaScript

JavaScript does not support Unicode for `\w`.  
Had to figure out another way.

Found **XRegExp** <https://github.com/slevithan/xregexp> which implements the Unicode range generators created by Mathias Bynens <https://github.com/mathiasbynens/unicode-data>

### Pattern

`/^(([\p{L}\p{N}!#$%&\'*+\-\/=?^_`\{|\}~]+(\.[\p{L}\p{N}!#$%&\'*+\-\/=?^_`\{|\}~]+)*|"[\p{L}\p{N}!#$%&\'*+\-\/=?^_`\{|\}~(),:;<>@\[\] \\"]+(\.[\p{L}\p{N}!#$%&\'*+\-\/=?^_`\{|\}~(),:;<>@\[\] \\"]+)*")@([\p{L}\p{N}^_-]+(\.[\p{L}\p{N}^_\-]+)*|\[[0-9.]+\]|\[IPv6:[A-Fa-f0-9:]+\]))$/ui`

*with `xregexp-all.js`, this becomes*

`'^(([\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~]+(\\.[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~]+)*|"[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~(),:;<>@\\[\\] \\\\"]+(\\.[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~(),:;<>@\\[\\] \\\\"]+)*")@([\\p{L}\\p{N}^_-]+(\\.[\\p{L}\\p{N}^_\\-]+)*|\\[[0-9.]+\\]|\\[IPv6:[A-Fa-f0-9:]+\\]))$'`

### Usage example

```js
var isValidEmail = function (mail) {
        var rx = XRegExp('^(([\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~]+(\\.[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~]+)*|"[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~(),:;<>@\\[\\] \\\\"]+(\\.[\\p{L}\\p{N}!#$%&\'*+\\-\\/=?^_`\\{|\\}~(),:;<>@\\[\\] \\\\"]+)*")@([\\p{L}\\p{N}^_-]+(\\.[\\p{L}\\p{N}^_\\-]+)*|\\[[0-9.]+\\]|\\[IPv6:[A-Fa-f0-9:]+\\]))$');

        return rx.test(mail);
    },
```


## Version

Version 0.1 -- 2017-03-06

## Copyright

Copyright © 2017 stoempDEV.com.  License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.  
This  is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.